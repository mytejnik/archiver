#include "Error.h"


const char * Arch::string_error(Error err) {
    switch (err) {
        case Error::OK:
            return "OK";
        case Error::ERROR:
            return "ERROR";
        case Error::NOT_IMPLEMENTED:
            return "NOT_IMPLEMENTED";
        case Error::WRONG_ARGUMENTS:
            return "WRONG_ARGUMENTS";
    }

    return "";
}
