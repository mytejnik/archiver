#pragma once

namespace Arch {

enum class Error {
    OK,
    ERROR,
    NOT_IMPLEMENTED,
    WRONG_ARGUMENTS,
};

const char * string_error(Error err);


}
