#include "Error.h"
#include "Huffman.h"

#include <istream>
#include <ostream>

using namespace Arch;

Error Arch::compress(std::istream *in, std::ostream *out) {
    if (in == nullptr || out == nullptr) {
        return Error::WRONG_ARGUMENTS;
    }

    return Error::OK;
}

Error Arch::decompress(std::istream *in, std::ostream *out) {
    if (in == nullptr || out == nullptr) {
        return Error::WRONG_ARGUMENTS;
    }

    return Error::OK;
}
