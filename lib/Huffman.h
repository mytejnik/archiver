#pragma once

#include "Error.h"

#include <istream>
#include <ostream>

namespace Arch
{

[[nodiscard]] Error compress(std::istream *in, std::ostream *out);

[[nodiscard]] Error decompress(std::istream *in, std::ostream *out);

}
